﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;

namespace Nimtone.Controllers
{
    public class MessagingController : Controller
    {
        //
        // GET: /Messaging/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Messaging/Details/5

        public ActionResult Send(string phone, string message)
        {

            // THIS IS A DEMONSTRATION!!
            Twilio.TwilioRestClient twilio = new TwilioRestClient("???", "???");
            var msg = twilio.SendSmsMessage("+", "+", message);
            twilio.InitiateOutboundCall("+", "+", "https://demo.twilio.com/welcome/voice/");
            return View();
        }

        //
        // GET: /Messaging/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Messaging/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Messaging/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Messaging/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Messaging/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Messaging/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
