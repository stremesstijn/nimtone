namespace Nimtone.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requiredpatientname : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Patients", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Patients", "Cellphone", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Patients", "Cellphone", c => c.String());
            AlterColumn("dbo.Patients", "Name", c => c.String());
        }
    }
}
