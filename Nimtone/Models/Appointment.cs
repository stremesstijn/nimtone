﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nimtone.Models
{
    public class Appointment
    {
        public int Id { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime DateTime { get; set; }
        public string Location { get; set; }

        [Required]
        public int PatientId { get; set; }
        public Patient Patient { get; set; }

    }
}