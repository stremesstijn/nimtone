﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nimtone.Models
{
    public class Patient
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        [RegularExpression(@"^\+[0-9]{11}", ErrorMessage = "Enter a phone number in international format. eg: +12345678910")]
        public string Cellphone { get; set; }

    }
}