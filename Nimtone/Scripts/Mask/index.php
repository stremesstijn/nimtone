<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Aftellen...</title>
<link rel="stylesheet" media="all" type="text/css" href="http://stylesheet.css">


<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>

<script type="text/javascript">
			
// <![CDATA[
jQuery(function($) {
      $.mask.definitions['~']='[+-]';
      $('#date').mask('99/99/9999');
      $('#phone').mask('(999) 999-9999');
      $('#phoneext').mask("(999) 999-9999? x99999");
      $("#tin").mask("99-9999999");
      $("#ssn").mask("999-99-9999");
      $("#product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});
      $("#eyescript").mask("~9.99 ~9.99 999");
   });
// ]]&gt;

</script>

</head>

<body>


						<div>
<input id="date" tabindex="1" type="text" placeholder="dd/mm/yyyy">
					</div>
<script type="text/javascript" src="jquery.maskedinput-1.3.1.min.js"></script>

</body>

</html>
